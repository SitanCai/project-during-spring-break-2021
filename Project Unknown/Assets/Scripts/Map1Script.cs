﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Map1Script : MonoBehaviour
{
    private float LorR = 0f; // Left or Right
    private float rotation = 0f;
    private float changeTime = 0f;
    // Start is called before the first frame update
    void Start()
    {
        LorR = Input.GetAxisRaw("Horizontal");
    }

    // Update is called once per frame
    void Update()
    {
        LorR = Input.GetAxisRaw("Horizontal");
        if (LorR == 0) 
        {
            changeTime -= Time.deltaTime;
            return; 
        }
        if (LorR != 0 && changeTime <= 0)
        {
            rotation += LorR * 90;
            transform.rotation = Quaternion.Euler(0, 0, rotation);
            Debug.Log(transform.rotation.eulerAngles.z);
            if (transform.rotation.eulerAngles.z > -1 && transform.rotation.eulerAngles.z < 1)
            {
                Physics2D.gravity = new Vector2(0, -9.81f);
            }else if (transform.rotation.eulerAngles.z > 89 && transform.rotation.eulerAngles.z < 91)
            {
                Physics2D.gravity = new Vector2(9.81f, 0);
            }else if (transform.rotation.eulerAngles.z > 269 && transform.rotation.eulerAngles.z < 271)
            {
                Physics2D.gravity = new Vector2(-9.81f, 0);
            }else if (transform.rotation.eulerAngles.z < 181 && transform.rotation.eulerAngles.z > 179)
            {
                Physics2D.gravity = new Vector2(0, 9.81f);
            }
            changeTime = 0.5f;
        }
        else
        {
            changeTime -= Time.deltaTime;
        }
    }
}
